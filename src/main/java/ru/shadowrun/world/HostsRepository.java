package ru.shadowrun.world;

import ru.shadowrun.domain.Host;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by HEDIN on 24.09.2016.
 */
public class HostsRepository {
    private static HostsRepository ourInstance = new HostsRepository();

    public static HostsRepository getInstance() {
        return ourInstance;
    }

    private Map<Long, Host> hosts = new HashMap<>();

    private HostsRepository() {

    }

    public void addHost(String name){
        Host host= new Host();
        host.setName(name);
        host.setId(hosts.size()+1);
        hosts.put(host.getId(), host);
    }

    public Collection<Host> getAllHosts(){
        return hosts.values();
    }

}
