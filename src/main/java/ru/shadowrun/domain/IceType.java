package ru.shadowrun.domain;

/**
 * @author azhulitov on 20.06.16.
 */
public enum IceType {
  Probe, Killer
}
