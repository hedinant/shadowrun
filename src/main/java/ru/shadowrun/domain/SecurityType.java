package ru.shadowrun.domain;

/**
 * @author azhulitov on 14.06.16.
 */
public enum SecurityType {
  Blue, Green, Orange, Red;
}
