package ru.shadowrun.domain;

/**
 * @author azhulitov on 20.06.16.
 */
public class SecurityTrigger {

  private int level;
  private IceType iceType;
  private int iceStrength;
  private Host host;
  private AlertLevel alertLevel;
}
