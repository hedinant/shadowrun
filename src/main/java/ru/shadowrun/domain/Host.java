package ru.shadowrun.domain;

import java.util.List;

/**
 * @author azhulitov on 14.06.16.
 */
public class Host {
    private long id;
    private String name;
    private HostType type;
    private int access;
    private int controll;
    private int file;
    private int index;
    private int slave;
    private SecurityType securityType;
    private int securityLevel;

    private List<Long> linkedNodes;
    private List<SecurityTrigger> triggers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HostType getType() {
        return type;
    }

    public void setType(HostType type) {
        this.type = type;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public int getControll() {
        return controll;
    }

    public void setControll(int controll) {
        this.controll = controll;
    }

    public int getFile() {
        return file;
    }

    public void setFile(int file) {
        this.file = file;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getSlave() {
        return slave;
    }

    public void setSlave(int slave) {
        this.slave = slave;
    }

    public SecurityType getSecurityType() {
        return securityType;
    }

    public void setSecurityType(SecurityType securityType) {
        this.securityType = securityType;
    }

    public int getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(int securityLevel) {
        this.securityLevel = securityLevel;
    }

    public List<Long> getLinkedNodes() {
        return linkedNodes;
    }

    public void setLinkedNodes(List<Long> linkedNodes) {
        this.linkedNodes = linkedNodes;
    }

    public List<SecurityTrigger> getTriggers() {
        return triggers;
    }

    public void setTriggers(List<SecurityTrigger> triggers) {
        this.triggers = triggers;
    }
}
