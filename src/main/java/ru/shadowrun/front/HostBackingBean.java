package ru.shadowrun.front;

import ru.shadowrun.domain.Host;
import ru.shadowrun.world.HostsRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author azhulitov on 14.06.16.
 */
public class HostBackingBean {

    public Collection<Host> getHosts() {
        return HostsRepository.getInstance().getAllHosts();
    }

    public void setRequest(HttpServletRequest request) {
        if ("add".equals(request.getParameter("submit")))
            HostsRepository.getInstance().addHost(request.getParameter("hostName"));
    }

}
